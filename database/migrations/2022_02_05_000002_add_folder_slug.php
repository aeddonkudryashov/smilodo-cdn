<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFolderSlug extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cloudinary_folders', function (Blueprint $table) {
            $table->string('slug')->index();
        });

        \Illuminate\Support\Facades\Artisan::call('update_folder_slug:start');

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cloudinary_folders', function (Blueprint $table) {
            $table->dropColumn('slug');
        });
    }
}
