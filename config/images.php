<?php

return [
    'default_resizes' => [
        [
            'quality' => 80,
            'width' => null,
            'height' => null,
        ],
        [
            'quality' => 80,
            'width' => 180,
            'height' => null,
        ],
        [
            'quality' => null,
            'width' => 230,
            'height' => null,
        ],
        [
            'quality' => 80,
            'width' => 270,
            'height' => null,
        ],
        [
            'quality' => 80,
            'width' => 360,
            'height' => null,
        ],
        [
            'quality' => 80,
            'width' => 375,
            'height' => null,
        ],
        [
            'quality' => 80,
            'width' => 420,
            'height' => null,
        ],
        [
            'quality' => 80,
            'width' => 540,
            'height' => null,
        ],
        [
            'quality' => 80,
            'width' => 640,
            'height' => null,
        ],
        [
            'quality' => 80,
            'width' => 667,
            'height' => null,
        ],
        [
            'quality' => 80,
            'width' => 720,
            'height' => null,
        ],
        [
            'quality' => 80,
            'width' => 768,
            'height' => null,
        ],
        [
            'quality' => 80,
            'width' => 960,
            'height' => null,
        ],
        [
            'quality' => 80,
            'width' => 1080,
            'height' => null,
        ],
        [
            'quality' => 80,
            'width' => 1140,
            'height' => null,
        ],
        [
            'quality' => 80,
            'width' => 1280,
            'height' => null,
        ],
        [
            'quality' => 80,
            'width' => 1366,
            'height' => null,
        ],
        [
            'quality' => 80,
            'width' => 1920,
            'height' => null,
        ]
    ]
];
