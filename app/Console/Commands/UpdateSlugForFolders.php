<?php

namespace App\Console\Commands;

use App\Models\CloudinaryFile;
use App\Models\CloudinaryFolder;
use Illuminate\Console\Command;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class UpdateSlugForFolders extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'update_folder_slug:start';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update folder slug';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        CloudinaryFolder::query()->chunk(1000, function ($folders){
           foreach ($folders as $folder){
                $folder->update(['slug' => Str::limit(Str::slug($folder->name,'_'),256)]);
           }
        });
    }
}
