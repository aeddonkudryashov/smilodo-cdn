<?php

namespace App\Console\Commands;

use App\Http\Controllers\FileController;
use App\Models\Article;
use App\Models\Category;
use App\Models\CloudinaryFile;
use App\Models\ContentObjectVersion;
use App\Models\CustomPageLanguage;
use App\Models\PredefinedSection;
use App\Models\Product;
use App\Models\ProductImages;
use Illuminate\Console\Command;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Storage;

class ResizeFiles extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'resize:files {--s|store_id=0} {--f|folder_id=0}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Resize files from original folder';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        if($this->option('store_id')){
            $store_id = $this->option('store_id');
            $files = Storage::allFiles(config('filesystems.folders.originals').'/'.$store_id) ;
            $progressBar = $this->output->createProgressBar(count($files));

            $progressBar->start();
            foreach ($files as $file) {
                try{
                    FileController::resizeFile(str_replace('originals/','',$file));
                }catch (\Exception $e){

                }

                $progressBar->advance();
            }

            $progressBar->finish();
        }

    }
}
