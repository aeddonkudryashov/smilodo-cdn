<?php

namespace App\Console\Commands;

use App\Http\Controllers\FileController;
use App\Models\Article;
use App\Models\Category;
use App\Models\CloudinaryFile;
use App\Models\ContentObjectVersion;
use App\Models\CustomPageLanguage;
use App\Models\PredefinedSection;
use App\Models\Product;
use App\Models\ProductImages;
use Illuminate\Console\Command;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Storage;

class CopyCloudinaryFiles extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cloudinary:download {--s|store_id=0} {--f|folder_id=0}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Download cloudinary files to storage';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $CloudinaryFiles = CloudinaryFile::query()->with('folder');

        if($this->option('folder_id')){
            $CloudinaryFiles->where('store_id',$this->option('store_id'));
        }

        if($this->option('store_id')){
            $CloudinaryFiles->where('store_id',$this->option('store_id'));
        }
        $this->info('Files count:'. $CloudinaryFiles->count());
        $CloudinaryFiles->each(function ($file, $key){
            $this->info('file =' . $key);
            $parents_path = '';
            if($file->folder_id){
                $parents_path = $file->folder->slug;
                $folder = $file->folder;
                while($folder->parent()->first()){
                    $parents_path = $file->folder->parent()->first()->slug.'/'.$parents_path;
                    $folder = $folder->parent()->first();
                }
            }

            $cloudinaryLink = 'http://res.cloudinary.com/aeddon/image/upload/q_80/'.$file->store_id.'/'.basename($file->cloudinary_name);
            $this->info('Processing ' . $cloudinaryLink . ' file');
            $new_path = config('filesystems.folders.originals'). '/'.$file->store_id.'/'.$parents_path.'/'.basename($file->cloudinary_name);
            if(!Storage::exists($new_path)){
                if(@file_get_contents($cloudinaryLink)){
                        Storage::put($new_path,file_get_contents($cloudinaryLink));
                        try{
                            FileController::resizeFile($file->store_id.'/'.$parents_path.'/'.basename($file->cloudinary_name));
                        }catch (\Exception $e){
                            $this->error('Error with transformations');
                        }
                        if($parents_path){
                            $this->info('folder '. $parents_path);
                            $new_name = $parents_path.'/'.basename($file->cloudinary_name);
                            Product::where('main_image',basename($file->cloudinary_name))->update(['main_image'=>$new_name]);
                            ProductImages::where('src',basename($file->cloudinary_name))->update(['src'=>$new_name]);
                            Article::where('thumb',basename($file->cloudinary_name))->update(['thumb'=>$new_name]);
                            Category::where('icon',basename($file->cloudinary_name))->update(['icon'=>$new_name]);
                            Category::where('image',basename($file->cloudinary_name))->update(['image'=>$new_name]);
                            CustomPageLanguage::where('og_image',basename($file->cloudinary_name))->update(['og_image'=>$new_name]);
                            PredefinedSection::where('picture',basename($file->cloudinary_name))->update(['picture'=>$new_name]);
                            $contentVersions = ContentObjectVersion::where('settings','like','%'.basename($file->cloudinary_name).'%')->get();
                            foreach($contentVersions as $contentVersion){
                                $contentVersion->settings = str_replace(basename($file->cloudinary_name),$new_name,$contentVersion->settings);
                                $contentVersion->save();
                            }
                            $file->update(['cloudinary_name' => $new_name]);
                        }
                        $this->info('Created new');
                }else{
                    $this->info('file not available');
                }
            }else{
                $this->info('file exists');
            }


        });
    }
}
