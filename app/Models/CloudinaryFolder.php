<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CloudinaryFolder extends Model
{
    use HasFactory;

    protected $primaryKey = 'id';

    protected $table = 'cloudinary_folders';

    public $timestamps = false;
    protected $fillable = [
        'slug'
    ];

    public function files(){
        return $this->hasMany(CloudinaryFile::class);
    }

    public function parent(){
        return $this->belongsTo(self::class,'parent_id');
    }
}
