<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CustomPageLanguage extends Model
{
    use HasFactory;

    protected $primaryKey = 'id';

    protected $table = 'custom_page_language';

    public $timestamps = false;

    protected $fillable = [
        'og_image'
    ];

}
