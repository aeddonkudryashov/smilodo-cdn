<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PredefinedSection extends Model
{
    use HasFactory;

    protected $primaryKey = 'id';

    protected $table = 'content_predefined_sections';

    public $timestamps = false;

    protected $fillable = [
        'picture'
    ];

}
