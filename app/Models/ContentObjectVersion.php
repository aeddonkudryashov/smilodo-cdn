<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ContentObjectVersion extends Model
{
    use HasFactory;

    protected $primaryKey = 'id';

    protected $table = 'content_objects_versions';

    public $timestamps = false;

    public $casts = [
    ];
    protected $fillable = [
        'picture'
    ];

}
