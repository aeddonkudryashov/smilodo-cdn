<?php

namespace App\Http\Middleware;

use App\Providers\RouteServiceProvider;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CDNenv
{
    /**
     * Change cdn env to needed
     */
    public function handle(Request $request, Closure $next)
    {
        if($request->has('env')){
            config([
                'filesystems.folders.originals' => $request->env .'/' .env('ORIGINAL_FILES','originals'),
                'filesystems.folders.resized' => $request->env .'/' .env('RESIZED_FILES','resized'),
            ]);
        }
        return $next($request);
    }
}
