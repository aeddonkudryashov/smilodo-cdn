<?php

namespace App\Http\Controllers;

use App\Jobs\ResizeFile;
use App\Jobs\UploadFile;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;
use Spatie\LaravelImageOptimizer\Facades\ImageOptimizer;

class FileController extends Controller
{
    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function upload(Request $request){

        if($request->has('async')){
            //add to queue for uploading (there will be resizing)
            UploadFile::dispatch($request);
        }else{
            $filepath = $this->upload_file($request);
            //resize queue
            ResizeFile::dispatch($filepath);
        }
        return  response()->json(['filepath' => str_replace(config('filesystems.folders.originals'). '/'.$request->store_id . '/','',$filepath)]);
    }

    public function upload_file($request){
        if(base64_encode(base64_decode($request->file, true)) === $request->file){
            $filepath = config('filesystems.folders.originals'). '/'.$request->store_id . '/'. ($request->path ? $request->path . '/': '').uniqid().'.jpg';
            Storage::put($filepath, base64_decode($request->file));
            return $filepath;
        }else{
            if(strpos($request->file,'http') !== false){
                $filepath = config('filesystems.folders.originals'). '/'.$request->store_id . '/'. ($request->path ? $request->path . '/': '').uniqid().'.jpg';
                Storage::put($filepath, file_get_contents($request->file));
                return $filepath;
            }else{
                return $request->file('file')->store(config('filesystems.folders.originals'). '/'.$request->store_id . '/'. ($request->path ?? ''));
            }
        }

    }

    /**
     * @param Request $request
     * @return array
     */
    public function delete(Request $request){
        $path = $request->path;
        if(!is_array($path)){
            $path = [$path];
        }
        foreach($path as $path_unit){
            //remove from originals
            Storage::delete(config('filesystems.folders.originals') . '/' . $path_unit);

            //remove from cache
            foreach(Storage::directories(config('filesystems.folders.resized')) as $resized_directory){
                Storage::delete( $resized_directory. '/' . $path_unit);
            }
        }

        return ['success' => 1];
    }

    public function getFiles(Request $request){
       // return Storage::
    }


    /**
     * Resize image in different sized
     * @param $content
     * @param $filename
     * @return bool|string
     */
    public static function resizeFile($filepath)
    {
            //check if image bigger then needed
        foreach(config('images.default_resizes') as $resizes){
          //  try {
                $image_resize = Image::make(Storage::path(config('filesystems.folders.originals'). '/' . $filepath));

                $amount_string = [];
                if($resizes['quality']){
                    $amount_string[] = 'q_' . $resizes['quality'];
                }
                if($resizes['width']){
                    $amount_string[] = 'w_' . $resizes['width'];
                }
                if($resizes['height']){
                    $amount_string[] = 'h_' . $resizes['height'];
                }
                $resized_filepath = config('filesystems.folders.resized'). '/'.implode(',',$amount_string).'/' . $filepath;

                if(!Storage::exists($resized_filepath)){
                    $ext = pathinfo($resized_filepath, PATHINFO_EXTENSION);
                    if(in_array($ext, ['jpg','jpeg','png','bmp','gif']) && ($resizes['width'] || $resizes['height'])){
                        $image_resize->resize($resizes['width'], $resizes['height'], function ($constraint) {
                            $constraint->aspectRatio();
                        });
                    }
                    if(!is_dir(str_replace(basename($filepath),'',$resized_filepath))){
                        //create folder if not exists, coz Image works only with created folders on Windows for sure
                        Storage::makeDirectory(str_replace(basename($filepath),'',$resized_filepath));
                    }
                    $image_resize->save(Storage::path($resized_filepath));

                    if(in_array($ext, ['jpg','jpeg','png','bmp','gif'])) {
                        //compress image
                        //need to set quality here from resizes
                        ImageOptimizer::optimize(Storage::path($resized_filepath));
                    }
                }

          /*  } catch (\Exception $e) {
                Log::channel('filemanager')->warning('Resizing;'.Storage::path(config('filesystems.folders.resized'). '/w-'.$resizes['width'].',h-'.$resizes['height'].'/' . $filepath).', error = ' . $e->getMessage());
            }*/
        }
    }
}
