<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
   return \Illuminate\Support\Facades\Storage::url('originals/1/imported_images/content_item_1/lLORlC8JV1ZevpqBzAxLXcpmbNya11L4Gc07q3EW.png');
});
